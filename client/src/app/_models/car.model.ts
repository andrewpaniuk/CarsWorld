export class Car {

    car;
    model;
    year;
    engine_capacity;
    created;
    color;
    description;
    price;
    image_path;

    constructor() {
        this.car = '';
        this.model = '';
        this.year = '';
        this.engine_capacity = '';
        this.created = '';
        this.color = '';
        this.description = '';
        this.price = '';
        this.image_path = '';
    }
}
