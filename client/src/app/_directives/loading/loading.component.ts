import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { LoadingService } from '../../_services/loading.service';

@Component({
    selector: '.loading',
    templateUrl: './loading.component.html',
    styleUrls: ['./loading.component.sass']
})
export class LoadingComponent implements OnInit {

    @Input() id;
    private element: any;

    constructor(private el: ElementRef,
        private _loadingService: LoadingService) {
        this.element = el.nativeElement;
     }

    ngOnInit() {

        const loading = this;

        if (!this.id) {
            console.log(`Loading component needs some id`);
        }

        this._loadingService.add(loading);
        // console.log(this.id)
    }

    ngOnDestroy(): void {
        this._loadingService.remove(this.id);
    }

    open() {
        this.element.style.display = 'block';
    }

    close() {
        this.element.style.display = 'none';
    }

}
