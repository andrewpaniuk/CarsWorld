import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class CarsService {

    constructor(private _http: HttpClient,
        private _route: ActivatedRoute) { }

    private apiURL = 'http://localhost:3000';
    private carsURL = `${this.apiURL}/api/cars`;

    carsChanged = new Subject()

    private cars: any;

    getCar(id: string) {
        return this._http.get(`${this.carsURL}/${id}`)
            .pipe(map(car => {
                console.log(car);
                return car;
            }))
    }

    setCars(cars) {
        this.cars = cars;
        this.carsChanged.next(this.cars.slice());
    }

    getCars() {
        const params = this._route.snapshot.queryParams
        this._http.get(`${this.carsURL}?car=${params.car}&model=${params.model}&color=${params.color}&year=${params.year}`)
            .pipe(map(cars => {
                return cars;
            }))
            .subscribe(cars => {
                this.setCars(cars);
            });
    }

    addCar(car) {
        return this._http.post(`${this.carsURL}/add`, car);
    }

    deleteCar(id) {
        return this._http.delete(`${this.carsURL}/${id}`);
    }

    editCar(id, car) {
        return this._http.post(`${this.carsURL}/${id}`, car)
            .pipe(map(car => {
                return car;
            }))
    }

}
