import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class LoadingService {

    loadings = [];

    constructor() { }

    add(loading: any) {
        this.loadings.push(loading);
    }

    remove(id) {
        this.loadings = this.loadings.filter(x => x.id !== id);
    }

    open(id) {
        const loading = this.loadings.find(x => x.id == id);
        loading.open();
    }

    close(id) {
        const loading = this.loadings.find(x => x.id == id);
        loading.close();
    }
}
