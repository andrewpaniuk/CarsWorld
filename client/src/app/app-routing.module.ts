import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CarsComponent } from './cars/cars.component';
import { CarsListComponent } from './cars/cars-list/cars-list.component';
import { CarsDetailComponent } from './cars/cars-detail/cars-detail.component';
import { CarsEditComponent } from './cars/cars-edit/cars-edit.component';
import { CarsFormComponent } from './cars/cars-form/cars-form.component';
import { CarsSearchComponent } from './cars/cars-search/cars-search.component';

const routes: Routes = [
    { path: '', redirectTo: '/search', pathMatch: 'full' },
    { path: 'search', component: CarsSearchComponent },
    { path: 'add', component: CarsFormComponent },
    { path: 'cars', component: CarsComponent },
    { path: 'cars/:id', component: CarsDetailComponent },
    { path: 'cars/:id/edit', component: CarsEditComponent }
];

export const app_routes = [
    CarsComponent,
    CarsListComponent,
    CarsDetailComponent,
    CarsEditComponent,
    CarsFormComponent,
    CarsSearchComponent
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
