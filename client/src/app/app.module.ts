import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';

import { AppRoutingModule, app_routes } from './app-routing.module';
import { CarsItemComponent } from './cars/cars-list/cars-item/cars-item.component';
import { CarsService } from './_services/cars.service';
import { LoadingComponent } from './_directives/loading/loading.component';
import { LoadingService } from './_services/loading.service';


@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        ...app_routes,
        CarsItemComponent,
        LoadingComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        AppRoutingModule
    ],
    providers: [CarsService, LoadingService],
    bootstrap: [AppComponent]
})
export class AppModule { }
