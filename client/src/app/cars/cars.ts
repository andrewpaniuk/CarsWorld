export const cars = [
    {
        index: 0,
        name: 'BWM',
        value: 'bmw',
        models: ['320', '520', '730', 'X5']
    },
    {
        index: 1,
        name: 'HONDA',
        value: 'honda',
        models: ['q', 'w', 'e', 'r']
    }
]

