import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';

import { cars } from '../cars'
import { colors } from '../colors'
import { getYears } from '../years'
import { CarsService } from '../../_services/cars.service';
import { Router } from '@angular/router';
import { Car } from '../../_models/car.model';

import { FileUploader } from 'ng2-file-upload/ng2-file-upload';

@Component({
    selector: 'app-cars-form',
    templateUrl: './cars-form.component.html',
    styleUrls: ['./cars-form.component.sass']
})
export class CarsFormComponent implements OnInit {

    @ViewChild('f') form: NgForm;

    cars;
    colors;
    years;


    selectedCar;
    selectedModel;
    selectedColor;
    selectedYear;
    models;

    selectedFile = null;

    constructor(private _carService: CarsService,
        private _router: Router,
        private el: ElementRef) { }

    ngOnInit() {
        this.cars = cars;
        this.colors = colors;

        this.years = getYears();
    }

    onChange() {
        if (this.selectedCar) {
            this.models = this.cars.find(car => car.value == this.selectedCar).models;
        }
    }

    onChangeFile(e) {
        console.log(e.target.files[0]);
        this.selectedFile = e.target.files[0];
    }

    onSubmit() {
        const values = this.form.value;
        const fd = new FormData();
        fd.append('image', this.selectedFile, this.selectedFile.name);
        fd.append('car', values.car);
        fd.append('model', values.model);
        fd.append('year', values.year);
        fd.append('engine_capacity', values.engine_capacity);
        fd.append('color', values.color);
        fd.append('description', values.description);
        fd.append('price', values.price);

        this._carService.addCar(fd)
            .subscribe((car: any) => {
                if(car) {
                    this._router.navigate([`/cars/${car._id}`])
                }
            });

    }

}
