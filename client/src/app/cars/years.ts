export function getYears(startYear?) {
    var currentYear = new Date().getFullYear(), years = [];
    startYear = startYear || 2000;

    while (startYear <= currentYear) {
        years.push(startYear++);
    }

    return years.reverse();
};
