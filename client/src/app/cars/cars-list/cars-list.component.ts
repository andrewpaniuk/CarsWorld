import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CarsService } from '../../_services/cars.service';
import { LoadingService } from '../../_services/loading.service';

@Component({
    selector: 'app-cars-list',
    templateUrl: './cars-list.component.html',
    styleUrls: ['./cars-list.component.sass']
})
export class CarsListComponent implements OnInit {

    @ViewChild('loading') loading: ElementRef;
    @ViewChild('info') info: ElementRef;

    cars: any = [];
    sort = 'new';

    params = this._route.snapshot.queryParams;

    constructor(private _route: ActivatedRoute,
        private _carService: CarsService,
        private _router: Router,
        private _loadingService: LoadingService) { }

    ngOnInit() {
        // const params = this._route.snapshot.queryParams;
        this._carService.getCars();
        this._carService.carsChanged
            .subscribe((cars: any) => {
                this.cars = cars.sort(function(a, b) {

                    if (a.created < b.created) {
                        return 1;
                    }
                    if (a.created > b.created) {
                        return -1;
                    }
                    return 0;

                });
                this._loadingService.close('loading-cars-list');
            });

    }

    onChange() {

        if (this.sort == 'new') {

            this.cars.sort(function(a, b) {

                if (a.created < b.created) {
                    return 1;
                }
                if (a.created > b.created) {
                    return -1;
                }
                return 0;

            });

        };

        if (this.sort == 'etoc') {

            this.cars.sort(function(a, b) {

                if (a.price < b.price) {
                    return 1;
                }
                if (a.price > b.price) {
                    return -1;
                }
                return 0;

            });
        };
        if (this.sort == 'ctoe') {

            this.cars.sort(function(a, b) {

                if (a.price > b.price) {
                    return 1;
                }
                if (a.price < b.price) {
                    return -1;
                }
                return 0;

            });
        };

    }

}
