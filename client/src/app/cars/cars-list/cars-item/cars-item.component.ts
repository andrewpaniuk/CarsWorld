import { Component, OnInit, Input } from '@angular/core';
import { CarsService } from '../../../_services/cars.service';
import { Router } from '@angular/router';

@Component({
    selector: '.car-item',
    templateUrl: './cars-item.component.html',
    styleUrls: ['./cars-item.component.sass']
})
export class CarsItemComponent implements OnInit {

    @Input() car;

    created;
    carCreated;

    constructor(private _carService: CarsService,
        private _router: Router) {

    }

    ngOnInit() {
        this.car.description = this.truncate(this.car.description, 80);
        this.created = new Date(this.car.created);
        this.carCreated = {
            hours: this.created.getHours(),
            minutes: this.created.getMinutes(),
            seconds: this.created.getSeconds(),
            year: this.created.getFullYear(),
            month: this.created.getMonth(),
            date: this.created.getDate(),
            day: this.created.getDay()
        }

    }

    truncate(str, maxlength) {
        if (str.length > maxlength) {
            let text = str.slice(0, maxlength - 3) + '...';
            return text;
        } else {
            return str;
        }
    }

    onShowDetail() {
        this._router.navigate(['/cars', this.car._id]);
    }

    onDelete() {
        const question = confirm('Are you sure?');
        switch (question) {
            case true:
                this._carService.deleteCar(this.car._id)
                    .subscribe(deletedCar => {
                        this._carService.getCars();
                    });
                break;
            default:
                break;
        }
    }
    onEdit() {
        this._router.navigate([`/cars/${this.car._id}/edit`])
    }

}
