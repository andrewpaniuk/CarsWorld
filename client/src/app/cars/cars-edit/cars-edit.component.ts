import { Component, OnInit, ViewChild } from '@angular/core';
import { CarsService } from '../../_services/cars.service';
import { ActivatedRoute, Router } from '@angular/router';


import { cars } from '../cars'
import { colors } from '../colors'
import { getYears } from '../years'
import { NgForm } from '@angular/forms';

@Component({
    selector: 'app-cars-edit',
    templateUrl: './cars-edit.component.html',
    styleUrls: ['./cars-edit.component.sass']
})
export class CarsEditComponent implements OnInit {

    @ViewChild('f') form: NgForm;

    car;

    cars;
    colors;
    years;

    selectedCar;
    selectedModel;
    selectedColor;
    selectedYear;

    models;

    constructor(private _carService: CarsService,
        private _route: ActivatedRoute,
        private _router: Router) {}

        ngOnInit() {

            this.cars = cars;
            this.colors = colors;

            this.years = getYears();


            const id = this._route.snapshot.paramMap.get('id');

            this._carService.getCar(id)
            .subscribe(car => {
                this.car = car;
                this.form.setValue({
                    car: this.car.car,
                    color: this.car.color,
                    engine_capacity: this.car.engine_capacity,
                    model: this.car.model,
                    year: this.car.year,
                    image: this.car.image_path,
                    description: this.car.description,
                    price: this.car.price
                })
                this.models = this.cars.find(car => car.value == this.selectedCar).models;
            })

    }

    onChange() {
        this.models = this.cars.find(car => car.value == this.selectedCar).models;
        this.selectedModel = '';
        console.log(this.models)
    }

    getModels() {
        this.cars.forEach(el => {
            if (el.value == this.selectedCar) {
                console.log(el)
                return el;
            }
        })
    }

    onSubmit() {
        const id = this.car._id;
        const car = this.form.value;

        this._carService.editCar(id, car)
            .subscribe((car: any) => {
                if (car) {
                    this._router.navigate([`/cars/${car._id}`])
                }
            });
    }

}
