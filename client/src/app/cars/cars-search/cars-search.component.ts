import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { cars } from '../cars'
import { colors } from '../colors'
import { getYears } from '../years'

@Component({
    selector: 'app-cars-search',
    templateUrl: './cars-search.component.html',
    styleUrls: ['./cars-search.component.sass']
})
export class CarsSearchComponent implements OnInit {

    @ViewChild('f') form: NgForm;

    cars;
    colors;
    years;

    selectedCar: any = 'all';
    selectedModel = 'all';
    selectedColor = 'all';
    selectedYear: any = 'all';

    models;


    constructor(private _router: Router) { }

    ngOnInit() {
        this.cars = cars;
        this.colors = colors;

        this.years = getYears();

    }

    onChange() {
        if (this.selectedCar != 'all') {
            this.models = this.cars.find(car => car.value == this.selectedCar).models;
        }
        this.selectedModel = 'all';
    }

    getModels() {
        this.cars.forEach(el => {
            if (el.value == this.selectedCar) {
                console.log(el)
                return el;
            }
        })
    }

    onSubmit() {
        // this._router.navigate(['/cars']);
        this._router.navigate(['/cars'], { queryParams: { car: this.selectedCar, model: this.selectedModel, color: this.selectedColor, year: this.selectedYear}})
    }

}
