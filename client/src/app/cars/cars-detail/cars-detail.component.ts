import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CarsService } from '../../_services/cars.service';

@Component({
    selector: 'app-cars-detail',
    templateUrl: './cars-detail.component.html',
    styleUrls: ['./cars-detail.component.sass']
})
export class CarsDetailComponent implements OnInit {

    car;
    sub;
    created;
    carCreated;

    constructor(private _route: ActivatedRoute,
        private _carService: CarsService,
        private _router: Router) { }

    ngOnInit() {
        this._route.params.subscribe((params) => {
            let id = params.id;
            this.sub = this._carService.getCar(id)
                .subscribe(car => {
                    this.car = car
                    this.created = new Date(this.car.created);
                    this.carCreated = {
                        hours: this.created.getHours(),
                        minutes: this.created.getMinutes(),
                        seconds: this.created.getSeconds(),
                        year: this.created.getFullYear(),
                        month: this.created.getMonth(),
                        date: this.created.getDate(),
                        day: this.created.getDay()
                    }
                });
            })
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    onDelete() {
        const question = confirm('Are you sure?');
        switch (question) {
            case true:
                this._carService.deleteCar(this.car._id)
                    .subscribe(deletedCar => {
                        this._router.navigate(['/search']);
                    });
                break;
            default:
                break;
        }
    }
    onEdit() {
        this._router.navigate([`/cars/${this.car._id}/edit`])
        // this._carService.editCar(this.car._id, car);
    }

}
