const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const car_schema = new Schema({
    car: {
        type: String,
        requared: true
    },
    model: {
        type: String,
        requared: true
    },
    created: {
        type: Date,
        default: new Date()
    },
    year: {
        type: Number,
        requared: true
    },
    color: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    image_path: {
        type: String,
        required: true
    },
    engine_capacity: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('cars', car_schema);
