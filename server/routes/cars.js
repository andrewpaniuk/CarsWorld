const express = require('express');
const path = require('path');
const router = express.Router();

const multer = require('multer');

const Car = require('../models/car.model');


const storage = multer.diskStorage({
    filename: function(req, file, cb) {
        cb(null, new Date().toISOString() + file.originalname);
    },
    destination: function(req, file, cb) {
        cb(null, path.join(__dirname + '/../public/uploads/'));
    }
});

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/jpg') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});


router.get('/cars', (req, res) => {
    console.log(req.query);

    let search = {};
    if (req.query.car != 'all') {
        search.car = req.query.car;
    }
    if (req.query.model != 'all') {
        search.model = req.query.model;
    }
    if (req.query.year != 'all') {
        search.year = req.query.year;
    }
    if (req.query.color != 'all') {
        search.color = req.query.color;
    }

    // let sort;
    // switch (req.query.sort) {
    //     case 'new':
    //         sort = '-created';
    //         break;
    //     case 'etoc':
    //         sort = '-price';
    //         break;
    //     case 'ctoe':
    //         sort = 'price';
    //         break;
    //     default:
    //         break;
    // }

    Car.find(search)
        // .sort(sort)
        .then(cars => {
            res.json(cars);
        });
});

router.get('/cars/:id', (req, res) => {

    Car.findById(req.params.id)
        .then(car => {
            console.log(car);
            res.json(car);
        });

});

router.post('/cars/add', upload.single('image'), (req, res) => {

    const MAIN_URL = 'http://localhost:3000'

    req.body.image = req.file.path.split('public')[1];

    const newCar = new Car();
    newCar.car = req.body.car;
    newCar.model = req.body.model;
    newCar.year = req.body.year;
    newCar.engine_capacity = req.body.engine_capacity;
    newCar.image_path = `${MAIN_URL}${req.body.image}`;
    newCar.color = req.body.color;
    newCar.price = req.body.price;
    newCar.description = req.body.description;
    newCar.created = new Date();

    newCar.save()
        .then(car => {
            res.json(car);
        });
});

router.delete('/cars/:id', (req, res) => {

    Car.findByIdAndRemove(req.params.id, function(err, post) {
        if (err) return next(err);
        res.json(post);
    });

});
router.post('/cars/:id', (req, res) => {

    const editedCar = {};
    editedCar.car = req.body.car;
    editedCar.model = req.body.model;
    editedCar.year = req.body.year;
    editedCar.engine_capacity = req.body.engine_capacity;
    editedCar.image_path = req.body.image;
    editedCar.color = req.body.color;
    editedCar.price = req.body.price;
    editedCar.description = req.body.description;
    editedCar.created = new Date();

    console.log(req.params.id)

    Car.update({
        _id: req.params.id
    }, editedCar, (error) => {
        if (error) {
            return console.log(error);
        }
        Car.findById({
                _id: req.params.id
            })
            .then(car => res.json(car));
    });

});




module.exports = router;
